#include <iostream>
#include <windows.h>
#include <time.h>
#include "src/Render.h"
#include "include/libtcod.hpp"



using namespace std;


int main()
{
    bool QuitGame=0;
    int a = 1;
    srand(time(NULL));
    FrameBufferClass *WorldBuffer = new FrameBufferClass();
//    FrameBufferClass *PathingBuffer = new FrameBufferClass();
    CreatureClass *mainchar = new CreatureClass();
    TCODConsole::setCustomFont("terminal16x16_gs_ro.png",TCOD_FONT_LAYOUT_ASCII_INROW | TCOD_FONT_TYPE_GRAYSCALE);
/*
    // Checking that the resolution is within bounds
    if (xRes > 150){xRes = 150;}
    if (xRes < 30){xRes = 30;}
    if (yRes > 150){xRes = 150;}
    if (yRes < 30){xRes = 30;}*/

    TCODConsole::initRoot(X_RES_DEFINE,Y_RES_DEFINE,"Testing",false);

//Testing
//    WorldBuffer->GrassTest(0,X_RES_DEFINE,0,Y_RES_DEFINE,150); //make Grass (startx, endx, starty, endy, number of tiles to change)
//    WorldBuffer->BuildRoomWall(10,30,5,15,TILE_WALL,TILE_FLOOR); //Make a building
    WorldBuffer->SetTile(testx,testy,TILE_FLOOR); //Make a door to the building

std::cout << "Tile at " << testx << ", " << testy << " is: " << WorldBuffer->GetMyTile(testx,5)->getDisplayChar() << "\n";


       TCODConsole::root->clear();
std::cout << "Root Clear = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";
       RebuildBuffer(WorldBuffer);
std::cout << "RebuildBuffer = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";
       TCODConsole::flush();
std::cout << "Flush = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";


   while ( !TCODConsole::isWindowClosed() || QuitGame == 1 ) {
std::cout << "1 = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";
       TCOD_key_t key;
std::cout << "2 = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";
       TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS,&key,NULL);
std::cout << "3 = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";

 //      UserInput(mainchar,key.vk);

       TCODConsole::root->clear();
std::cout << "4 = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";
       RebuildBuffer(WorldBuffer);
std::cout << "5 = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";
 //      RenderCreature(mainchar);
       TCODConsole::flush();
std::cout << "6 = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";
       if (a==1){break;}
       a=1;
   }
std::cout << "After Quit = " << WorldBuffer->GetMyTile(testx,testy)->getDisplayChar() << "\n";


    return 0;
}

