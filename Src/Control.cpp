#include <iostream>
#include <windows.h>
#include <time.h>
#include "Render.h"
#include "../include/libtcod.hpp"



void UserInput(CreatureClass *mainchar,int Key)
{
    switch(Key)
    {
    case TCODK_UP : mainchar->yPos --; break;
    case TCODK_DOWN : mainchar->yPos ++; break;
    case TCODK_LEFT : mainchar->xPos --; break;
    case TCODK_RIGHT : mainchar->xPos ++; break;
    default:break;
    }
}


