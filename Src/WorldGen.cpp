#include <iostream>
#include <windows.h>
#include "Render.h"


void FrameBufferClass::GrassTest(int sx,int ex, int sy,int ey, int quant) //sx,sy start of the square area ex,ey specify the end of the area, quant gives the number of shrubs to add
{
   int a;
   for(a = 0; a < quant ; a++)
    {
        if((rand()%2) < 1){SetTile(sx + rand()%(ex-sx) ,sy + rand()%(ey-sy),TILE_GRASS1 );}
        //else if((rand()%2) < 1){SetTile(sx + rand()%(ex-sx) ,sy + rand()%(ey-sy),TILE_GRASS1 );}
        else {SetTile(sx + rand()%(ex-sx) ,sy + rand()%(ey-sy),TILE_GRASS2 );}
    }
}

void FrameBufferClass::BuildRoomWall(int sx,int ex, int sy,int ey, TileType WallType, TileType FloorType) //sx,sy start of the square area ex,ey specify the end of the area, quant gives the number of shrubs to add
{
   int a,b;

   if (sx+2 >= ex){return;} //sanity checks, make sure this is a build-able building, must be 3x3 or larger
   if (sy+2 >= ey){return;}

   for(a = sx; a <= ex ; a++)
    {
        SetTile(a, sy, WallType);
    }
   for(a = sy; a <= ey ; a++)
    {
        SetTile(sx, a, WallType);
    }

   for(a = sx; a <= ex ; a++)
    {
        SetTile(a, ey, WallType);
    }

   for(a = sy; a <= ey ; a++)
    {
        SetTile(ex, a, WallType);
    }

   for(a = sx+1; a < ex; a++)
    {
       for(b = sy+1; b < ey; b++)
       {
          SetTile(a, b, FloorType);
       }
    }


    return;
}
