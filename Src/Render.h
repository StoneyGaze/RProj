#ifndef RENDER_H_INCLUDED
#define RENDER_H_INCLUDED
#define X_RES_DEFINE 80 //our bounds for window generation
#define Y_RES_DEFINE 40
#define testx 26
#define testy 14

class TileType {
  private:
    const char _displayChar;
    const bool _walkable;
  public:
    TileType(char displayChar, bool walkable) : _displayChar(displayChar), _walkable(walkable){}
    const bool isWalkable() const {
      return _walkable;
    }
    const char getDisplayChar() const {
      return _displayChar;
    }
};

const TileType TILE_WALL('#', false);
const TileType TILE_FLOOR('#', true);
const TileType TILE_SPACE('.', true);
const TileType TILE_GRASS1(',', true);
const TileType TILE_GRASS2('.', true);

class FrameBufferClass {
    private:
    const TileType *Tile[X_RES_DEFINE][Y_RES_DEFINE];

    public:
    FrameBufferClass();
    bool SetTile(int x, int y, TileType c);
    const TileType* GetMyTile(int x, int y);

//For World Gen Only
    void GrassTest(int sx,int ex, int sy,int ey, int quant);
    void BuildRoomWall(int sx,int ex, int sy,int ey, TileType WallType, TileType FloorType);
};

/*
class TileClass
{
private:
    bool Walkable; //Sets if any creature can WALK over the tile
};*/

//Creature Class
class CreatureClass {
    public:
        int xPos = 0, yPos = 0;
        char icon = '@';
};

bool RebuildBuffer(FrameBufferClass *WorldBuffer);
void RenderCreature(CreatureClass *mainchar);
void UserInput(CreatureClass *mainchar,int Key);

#endif // RENDER_H_INCLUDED
